# Wiki Summary App

## Overview

The Wiki Summary App is a Python program that provides a 500-word summary for any word entered. It leverages online sources to generate concise and informative summaries for quick reference.

## Features

- Obtain a 500-word summary for any word.
- Simple command-line interface for easy interaction.
- Retrieve a list of sections from a Wikipedia page.


## Installation

To use the Wiki Summary App, follow these steps:

1. Clone the repository:

    ```bash
    git clone https://gitlab.com/ai6180204/AIDI-2004.git
    ```

## Usage

Run the following command to get a summary for a word:

```bash
python Lab1.py <word>
